# Phony2K Word List

A list of commonly used words that are not valid in NWL or in CSW.

Originally taken from [“2000+ Phoney words that should be words”](http://www.breakingthegame.net/blog), with some revisions.
